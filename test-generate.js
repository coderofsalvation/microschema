var merge    = require('./lib/merge')
var generate = require('json-schema-defaults')

var json = generate( require('./schema/1.1.js') )
json.events[0].schemas = [
  { schema: "event/rpc/1.3"      },
  { schema: "event/rpc/http/1.1" }
]
json.events[0] = merge( json.events[0], generate( require('./schema/event/rpc/1.3')      ) )
json.events[0] = merge( json.events[0], generate( require('./schema/event/rpc/http/1.1') ) )

console.log( JSON.stringify(json,null,2) )
