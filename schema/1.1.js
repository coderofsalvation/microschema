module.exports = {
  type: 'object',
  id: "1.1",
  properties: {
    version: {
      type: 'string',
      minLength: 1,
      default: "1.1"
    },

    service: {
      type: 'object',
      properties: {
        key:  { type: 'integer', required: true, default: 1 },
        name: { type: 'string' , required: true, default: "foobar" }
      }
    },

    events: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id:      { type: 'integer', min: 1, default:10 },
          parent:  { type: 'integer', min: 1, default:1  },
          schemas: { 
            type: 'array',
            items: {
              type: 'object',
              properties: {
                schema: { type:'string' }
              }
            } 
          }      
        }
      }

    }
  }
   
}
