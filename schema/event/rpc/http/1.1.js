module.exports = {
  type: "object",
  id: "event/rpc/http/1.1",
  properties: {
    cs: {
      type: "object",
      properties: {
        host: { type: "string", required: true, default: "bar" },
        url:  { type: "string", required: true, default: "/foo" },
        method: { 
          type: "string", 
          required: true, 
          default: "GET" 
        }
      }
    },
    cr: {
      type: "object",
      properties: {
        statusCode: { type: "integer", required: true, default: 404 },
        body:       { type: "string", required: true, default: "<html><body>..." }
      } 
    }
  }
}
