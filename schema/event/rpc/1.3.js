module.exports = {
  type: "object",
  id: "event/rpc/1.3",
  properties: {
    cs: {
      type: "object",
      properties: {
        timestamp: { type: "integer", required: true, min:100000 },
        status:    { 
          type: "string",
          enum: ["OK","ERROR","TIMEOUT"],
          default: "OK"
        } 
      }
    },
    cr: {
      type: "object",
      properties: {
        timestamp: { type: "integer", required: true, min:100000 },
        status:    { 
          type: "string",
          enum: ["OK","ERROR","TIMEOUT"],
          default: "OK"
        },
        serviceId: { type: "string", default: "734" }
      } 
    },
  }
}
