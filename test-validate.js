var validate = require('./lib/validate').validate

schemas = {
  '1.1'                : require('./schema/1.1.js'),
  'event/rpc/1.3'      : require('./schema/event/rpc/1.3.js'),
  'event/rpc/http/1.1' : require('./schema/event/rpc/http/1.1.js')
}

if( ! !!process.argv[2] ){
  console.log( "Usage: \n\tnode test-generate.js > payload.json\n\tnode test-validate.js ./payload.json" )
  process.exit(0)
}

var r = validate(require( process.argv[2] ))
console.log(JSON.stringify(r, null, 2))
