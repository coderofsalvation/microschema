module.exports = (function(){

  /**
   * Simple is object check.
   * @param item
   * @returns {boolean}
   */
  this.isObject = function (item) {
    return (item && typeof item === 'object' && !Array.isArray(item) && item !== null);
  }

  /**
   * Deep merge two objects.
   * @param target
   * @param source
   */
  this.merge = function (target, source) {
    if (this.isObject(target) && isObject(source)) {
      Object.keys(source).forEach(key => {
        if (isObject(source[key])) {
          if (!target[key]) Object.assign(target, { [key]: {} });
          this.merge(target[key], source[key]);
        } else {
          Object.assign(target, { [key]: source[key] });
        }
      });
    }
    return target;
  }

  return this.merge

})()
