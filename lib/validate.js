var tv4 = require('tv4')

module.exports = (function(){
 
  this.validate = function (payload, result) {
    if (!result){
      result = { valid: true, schemas: {} }
      result.schemas[ payload.version  ] = tv4.validateMultiple(payload, schemas[ payload.version ])
    } 
    if (!!payload.schemas) {
      payload.schemas.map( function(s){
        var schema = schemas[ s.schema ]
        if (!!schema) {
          var r = tv4.validateMultiple(payload, schema)
          result.schemas[ s.schema ] = r
          if (!r.valid) result.valid = false
        }
      })
    }
    for ( var k in payload)
      if( typeof payload[k] == "object" ) this.validate(payload[k], result)
    return result
  } 

  return this
})()
